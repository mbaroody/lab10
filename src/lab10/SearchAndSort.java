package lab10;

public class StopList extends GenericList_MWB<Stop> {

   public StopList() {
      super();
   }

   public StopList(int size) {
     super(size);
   } 
   
   public int sequentialSearch(Stop stop_in) {}
   public int binarySearch(Stop stop_in) {}
   public void selectionSort() {} 
}
