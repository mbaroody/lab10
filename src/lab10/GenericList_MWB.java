package lab10;

public class GenericList_MWB <T> {
	
	private static final int MAX = 100;
	
	protected Object [] array;
	protected int index;
	protected int pointer;

	public GenericList_MWB() {
		this.array = new Object [MAX];
		this.index = 0;
		this.pointer = 0;
	}
	
	public GenericList_MWB(int size) {
		this.array = new Object [size];
		this.index = 0;
		this.pointer = 0;
	}
	
	public GenericList_MWB(T [] anArr, int newIndex) {
		this.array = anArr;
		this.index = newIndex;
		this.pointer = 0;
	}
	
	public void reset() {
		this.pointer = 0;
	}
	
	public boolean hasNext() {
		if (this.pointer < this.index) 
			return true;
		else 
			return false;
	}
	
	@SuppressWarnings("unchecked")
	public T getNext() {
		return (T) this.array[this.pointer++];
	}

   public void setPointer(int in) {
      this.pointer = in;
   }

   public int getPointer() {
      return this.pointer;
   }

	public T[] getArray() {
		@SuppressWarnings("unchecked")
		T [] copy = (T[]) this.array;
		return copy;
	}
	
	public void setArray(T [] anArr) {
		this.array = anArr;
	}
	
	public int getIndex() {
		return this.index;
	}
	
	public void setIndex(int anIndex) throws IndexOutOfBoundsException {
		if (anIndex < 0 || anIndex >= this.array.length)
			throw new IndexOutOfBoundsException();
		else this.index = anIndex;
	}
	
	public Object getObject(int pos) throws IndexOutOfBoundsException {
		if (pos < 0 || pos >= this.array.length)
			throw new IndexOutOfBoundsException();
		else {
			Object copy = this.array[pos];
			return copy;
		}
	}
	
	public String toString() {
		String toString = "";
		for (int i = 0; i < this.index; i++) {
			toString = toString + this.array[i].toString() + "\n";
		}
		return toString;
	}
	
	public boolean equals(GenericList_MWB<T> anArr) {
		if (this.index == anArr.getIndex()
				&& this.array.length == anArr.getArray().length) {
			T [] otherArray = anArr.getArray();
			for (int i = 0; i < this.index; i ++) {
				if (!this.array[i].equals(otherArray[i]))
					return false;
			}
			return true;
		}
		
		else return false;
	}
	
	public void add(Object obj) {
		if (this.isFull()) 
			return;
		else {
			this.array[this.index] = obj;
			index++;
		}
	}
	
	public void delete(int pos) throws IndexOutOfBoundsException {
		if (pos < 0 || pos > this.array.length)
			throw new IndexOutOfBoundsException();
		else if (this.isEmpty() || pos >= this.index)
			return;
		else {
			Object temp1 = this.array[this.index - 1];
			Object temp2 = null;
			for (int i = this.index - 2; i >= pos; i--) {
				temp2 = this.array[i];
				this.array[i] = temp1;
				temp1 = temp2;
			}
			index--;
		}
	}
	
	public void delete(T anObj) {
		if (this.isEmpty())
			return;
		else {
			for (int i = 0; i < this.index; i++) {
				if (this.array[i].equals(anObj)) this.delete(i);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void insert(int pos, T anObj) throws IndexOutOfBoundsException {
		if (pos < 0 || pos > this.array.length)
			throw new IndexOutOfBoundsException();
		else if (this.isFull() || pos > this.index) 
			return;
		else if (pos == this.index) {
			this.add(anObj);
			return;
		} else {
			T temp1 = (T) this.array[pos];
			T temp2 = null;
			this.array[pos] = anObj;
			for (int i = pos + 1; i <= this.index; i++) {
				temp2 = (T) this.array[i];
				this.array[i] = temp1;
				temp1 = temp2;
			}
		}
		index++;
	}

   @SuppressWarnings("unchecked")
   public void swap(int index_a, int index_b) throws IndexOutOfBoundsException {
      if (index_a < 0 || index_b < 0) 
         throw new IndexOutOfBoundsException();
      else if (index_a >= this.index || index_b >= this.index) 
         return;
      T temp = (T) array[index_a];
      this.array[index_a] = this.array[index_b];
      this.array[index_b] = temp;
   }

	
	public int isThere(T anObj) {
		if (this.index == 0) return -1;
		else {
			for (int i = 0; i < this.index; i++) {
				if (this.array[i].equals(anObj)) return i;
			}
		}
		return -1;
	}
	
	public boolean isFull() {
		if (this.index >= this.array.length) return true;
		else return false;
	}
	
	public boolean isEmpty() {
		if (this.array.length == 0) return false;
		else if (this.index == 0) return true;
		else return false;
	}
	
	void clear() {
		this.setIndex(0);
	}
	
	@SuppressWarnings("unchecked")
	void trim() {
		T [] trimmed = (T[]) new Object [this.index];
		for (int i = 0; i < this.index; i++) {
			trimmed[i] = (T) this.array[i];
		}
		this.setArray(trimmed);
	}
	
	@SuppressWarnings("unchecked")
	void moreCapacity(int factor) {
		if (factor < 1) return;
		T [] enlarged = (T[]) new Object [this.array.length * factor];
		for (int i = 0; i < this.index; i++) {
			enlarged[i] = (T) this.array[i];
		}
		this.setArray(enlarged);
	}
	
	@SuppressWarnings("unchecked")
	void moreCapacity(double factor) {
		if (factor < 1.0) return;
		T [] enlarged = (T[]) new Object [(int)(this.array.length * factor)];
		for (int i = 0; i < this.index; i++) {
			enlarged[i] = (T) this.array[i];
		}
		this.setArray(enlarged);
	}
}
