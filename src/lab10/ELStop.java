package lab10;

public class ELStop extends Stop {
	
	private int stop_id;
	private int stop_code;
	protected GPSLocation gps;
	private int type;
	private int parent;
	private boolean WCA;
	//stands for wheel-chair accessible
	
	public ELStop() {
		this.stop_id = 0;
		this.stop_code = 0;
		this.gps = new GPSLocation();
		this.type = 0;
		this.parent = 0;
		this.WCA = false;
	}
	
	public ELStop(int id_in, int code_in, GPSLocation gps_in, int type_in, 
			int parent_in, boolean WCA_in) {
		this.stop_id = id_in;
		this.stop_code = code_in;
		this.gps = gps_in;
		this.type = type_in;
		this.parent = parent_in;
		this.WCA = WCA_in;
	}
	
	//copy constructor
	public ELStop(ELStop stop_in) {
		this.stop_id = stop_in.getStopID();
		this.stop_code = stop_in.getStopCode();
		this.gps = stop_in.getGPS();
		this.type = stop_in.getType();
		this.parent = stop_in.getParent();
		this.WCA = stop_in.isWCA();
	}
	
	public int getStopID() {
		return this.stop_id;
	}
	
	public int getStopCode() {
		return this.stop_code;
	}
	
	public GPSLocation getGPS() {
		return this.gps;
	}
	
	public int getType() {
		return this.type;
	}
	
	public int getParent() {
		return this.parent;
	}
	
	public boolean isWCA() {
		return this.WCA;
	}
	
	public void setStopID(int id_in) {
		this.stop_id = id_in;
	}
	
	public void setStopCode(int code_in) {
		this.stop_code = code_in;
	}
	
	public void setGPS(GPSLocation gps_in) {
		this.gps = gps_in;
	}
	
	public void setType(int type_in) {
		this.type = type_in;
	}
	
	public void setParent(int parent_in) {
		this.parent = parent_in;
	}
	
	public void setWCA(boolean WCA_in) {
		this.WCA = WCA_in;
	}
	
	public int calcCost() {
		return Stop.baseCost;
	}
	
	public boolean equals(Object o_in) {
		if (!(o_in instanceof ELStop))
			throw new ClassCastException("Expected ELStop");
		
		ELStop elstop_in = (ELStop) o_in;
		return this.stop_id == elstop_in.getStopID()
				&& this.stop_code == elstop_in.getStopCode()
				&& this.gps.equals(elstop_in.getGPS())
				&& this.type == elstop_in.getType()
				&& this.parent == elstop_in.getParent()
				&& this.WCA == elstop_in.isWCA();
	}
	
	public String toString() {
		return "ID: " + this.stop_id + 
				", code: " + (this.stop_code < 0 ? "n/a" : this.stop_code) + 
	            ", GPS: " + this.gps.toString() + 
	            ", type: " + (this.type < 0 ? "n/a" : this.type) +
	            ", parent: " + (this.parent < 0 ? "n/a" : this.parent) + 
	            ", wheelchair access? " + this.WCA;
	}
}
