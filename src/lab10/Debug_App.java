package lab10;

public class Debug_App {

   public static void main(String [] args) {
      
      ReadFile reader = new ReadFile("elStops_1.txt");
      StopList stopList = new StopList();
      reader.read(stopList);
      stopList.selectionSort();
      System.out.println(stopList.toString());
      reader.close();
      
   }
}
