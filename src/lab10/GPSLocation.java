package lab10;

public class GPSLocation {
	
	String name;
	double latitude;
	double longitude;
	
	public GPSLocation() {
		this.name = "South Atlantic";
		this.latitude = 0.0;
		this.longitude = 0.0;
	}
	
	public GPSLocation(String name_in, double lat_in, double long_in) {
		this.name = name_in;
		this.latitude = lat_in;
		this.longitude = long_in;
	}
	
	public String getName() {
		return this.name;
	}
	
	public double getLat() {
		return this.latitude;
	}
	
	public double getLong() {
		return this.longitude;
	}
	
	public void setName(String name_in) {
		this.name = name_in;
	}
	
	public void setLat(double lat_in) {
		this.latitude = lat_in;
	}
	
	public void setLong(double long_in) {
		this.longitude = long_in;
	}
	
	public boolean equals(GPSLocation gps_in) {
		if (this.name.equals(gps_in) 
				&& this.latitude == gps_in.getLat()
				&& this.longitude == gps_in.getLong())
			return true;
		else return false;
	}
	
	public String toString() {
		return this.name + " @ " + 
	           (this.latitude < 0 ? (this.latitude + " S, ") 
				: ("+" + this.latitude + " N, ")) + 
			   (this.longitude < 0 ? (this.longitude + " W")
				: ("+" + this.longitude + " E"));
	}

   public int compareTo(GPSLocation gps_in) {
      return this.name.compareTo(gps_in.getName());
   }
}
