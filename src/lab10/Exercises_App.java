package lab10;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

public class Exercises_App {

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		
		ReadFile reader = new ReadFile("elStops_0.txt");
		GenericList_MWB<ELStop> stopList = new GenericList_MWB<ELStop>();
		reader.read(stopList);
			
		int totalCost = 0;
		
		while (stopList.hasNext()) {
			Stop temp = stopList.getNext();
			String name = ((ELStop) temp).getGPS().getName();
			System.out.print(name + ", "); 
			int cost = ((ELStop) temp).calcCost();
			System.out.println("Cost: " + cost + "K"); //5c
			totalCost += cost;
		}
		
		System.out.println("\nTotal cost: " + totalCost + "K");
	}

}
