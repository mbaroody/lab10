package lab10;

public class StopList extends GenericList_MWB<ELStop> {

   public StopList() {
      super();
   }

   public StopList(int size) {
     super(size);
   } 
   
   public int sequentialSearch(ELStop stop_find) {
      GPSLocation target = stop_find.getGPS();
      ELStop temp;
      while (this.hasNext()) {
         temp = this.getNext();
         if (temp.getGPS().compareTo(target) == 0) {
            int loc = this.pointer - 1;
            this.reset();
            return loc;
         }
      }
      return -1;
   }

   public int binarySearch(ELStop stop_in) {
      GPSLocation target = stop_in.getGPS();
      int first = 0;
      int last = this.array.length - 1;
      while (first <= last) {
         int mid = (first + last) / 2;
         if (target.compareTo(( (ELStop) (getObject(mid))).getGPS()) > 0)
            first = mid + 1;
         else if (target.compareTo(( (ELStop)(getObject(mid))).getGPS()) < 0)
            last = mid - 1;
         else 
            return mid;
      }
      return -1;
   }

   public void selectionSort() {
      ELStop smallest = (ELStop) this.array[0];
      ELStop temp;
      int where;
      for (int i = 0; i < this.index; i++) {
         where = -1;
         smallest = (ELStop) this.array[i];
         temp = (ELStop) this.array[i];
         for (int j = i; j < this.index; j++) {
            if (( (ELStop)(this.array[j])).getGPS().compareTo(smallest.getGPS()) < 0) {
               smallest = (ELStop) this.array[j];
               where = j;
            }
         }
         if (where < 0) 
            continue;
         else {
            this.array[i] = smallest;
            this.array[where] = temp;
         }
      }
   } 
}
