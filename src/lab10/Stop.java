package lab10;

public abstract class Stop {
	
	protected static int baseCost = 5;
	protected static int minCost = 10;
	
	public abstract int calcCost();
	
	public abstract boolean equals(Object o);
	
	public abstract String toString();
}
