package lab10;

public class StopUnderConstruction extends ELStop {

	public static final String [] LEVEL_NAMES = {"should never print", "minor upgrade", "moderate upgrade", 
		"major upgrade", "almost complete re-do"};
	
	private int level;
	
	public StopUnderConstruction() {
		super();
		this.level = 0;
	}

	public StopUnderConstruction(int id_in, int code_in, GPSLocation gps_in,
			int type_in, int parent_in, boolean WCA_in, int level_in) {
		super(id_in, code_in, gps_in, type_in, parent_in, WCA_in);
		this.level = level_in;
	}
	
	public StopUnderConstruction(ELStop stop_in, int level_in) {
		super(stop_in);
		this.level = level_in;
	}
	
	public StopUnderConstruction(int id_in, int code_in, String name_in, 
			double lat_in, double long_in,int type_in, int parent_in, 
			boolean WCA_in, int level_in) {
		super(id_in, code_in, new GPSLocation(name_in, lat_in, long_in), 
				type_in, parent_in, WCA_in);
		this.level = level_in;
	}
	
	public int getLevel() {
		return this.level;
	}
	
	public void setLevel(int level_in) {
		this.level = level_in;
	}
	
	public int calcCost() {
		return this.level * Stop.minCost;
	}
	
	public boolean equals(Object o_in) {
		if (!(o_in instanceof StopUnderConstruction))
			throw new ClassCastException("Expected StopUnderConstruction.");
		
		StopUnderConstruction in = (StopUnderConstruction) o_in;
		return super.equals((ELStop) in) && 
				this.level == in.getLevel();
	}
	
	@SuppressWarnings("static-access")
	public String toString() {
		return super.toString() + 
				", construction status: " + this.LEVEL_NAMES[this.level];
	}

}
