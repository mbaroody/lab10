"Harlem/Lake Green Line Station", Cost: 20K
"Oak Park-Green", Cost: 5K
"Ridgeland", Cost: 5K
"Austin-Green", Cost: 40K
"Central-Green", Cost: 5K
"Laramie & Lake-Green", Cost: 30K
"Cicero-Green", Cost: 5K
"Pulaski-Green", Cost: 20K
"Kedzie-Green", Cost: 5K
"California-Green", Cost: 10K
"Ashland/63rd Green Line Station", Cost: 5K
"Clinton-Green", Cost: 20K
"Clark/Lake-Green", Cost: 5K
"State/Lake-Green", Cost: 30K
"Randolph/Wabash-Green", Cost: 5K
"Madison/Wabash-Green", Cost: 10K
"Adams/Wabash-Green", Cost: 5K
"Roosevelt Orange/Red/Green Line Station", Cost: 10K
"35-Bronzeville-IIT-Green", Cost: 5K
"Indiana Green Line Station", Cost: 20K
"43rd Green Line Station", Cost: 5K
"47th Green Line Station", Cost: 30K
"51st Green Line Station", Cost: 5K
"Garfield Green Line Station", Cost: 10K
"Fullerton Red/Brown/Purple Line Station", Cost: 5K
"69th Red Line Station", Cost: 10K
"Jarvis-Red", Cost: 5K
"Morse-Red", Cost: 40K
"Sheridan & Loyola-Red", Cost: 5K
"Sheridan & Granville-Red", Cost: 30K
"Sheridan & Thorndale-Red", Cost: 5K
"Bryn Mawr Red Line Station", Cost: 10K
"Sheridan & Berwyn-Red ", Cost: 5K
"Sheridan & Argyle-Red", Cost: 10K
"Lawrence Red Line Station", Cost: 5K
"4242 N Sheridan-Red", Cost: 30K
"Addison Red Line Station", Cost: 5K
"Sheridan & Belmont-Red", Cost: 20K
"Fullerton Red/Brown/Purple Line Station", Cost: 5K
"North Avenue & Clybourn/Halsted-Red", Cost: 20K
"Clark/Division-Red", Cost: 5K
"Chicago-Red", Cost: 20K
"Grand-Red", Cost: 5K
"State/Lake-Red", Cost: 30K
"Monroe-Red", Cost: 5K
"Jackson-Red", Cost: 10K
"State & Harrison/Congress-Red", Cost: 5K
"Roosevelt Orange/Red/Green Line Station", Cost: 10K
"Cermak-Chinatown Red Line Station", Cost: 5K
"Sox-35th Red Line Station", Cost: 10K
"47th Red Line Station", Cost: 5K
"Garfield Red Line Station", Cost: 20K
"63rd-Red", Cost: 5K
"69th Red Line Station", Cost: 20K
"79th Red Line Station", Cost: 5K
"87th Red Line Station", Cost: 20K
"95th Street Red Line Station", Cost: 5K
"Midway Orange Line Station", Cost: 30K
"Pulaski Orange Line Station", Cost: 5K
"Kedzie Orange Line Station", Cost: 10K
"Western Orange Line Station", Cost: 5K
"35th/Archer Orange Line Station", Cost: 10K
"Ashland Orange Line Station", Cost: 5K
"Halsted Orange Line Station", Cost: 20K
"Roosevelt Orange/Red/Green Line Station", Cost: 5K
"Van Buren & State-Orange", Cost: 30K
"LaSalle/Van Buren-Orange", Cost: 5K
"Quincy/Wells-Orange", Cost: 40K
"Washington/Wells-Orange", Cost: 5K
"Clark/Lake-Orange (Subway)", Cost: 5K
"State/Lake-Orange", Cost: 5K
"Randolph/Wabash-Orange", Cost: 5K
"Madison/Wabash-Orange", Cost: 5K
"Adams/Wabash-Orange", Cost: 5K


Total cost: 900K
